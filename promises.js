
function echo(msg){
    return new Promise((resolve)=>{

        setTimeout(()=>{
            resolve(msg)
        },2000)
    })
}

p = echo('Ala')

p2 = p.then( w => w + ' ma ')

p3 = p2.then( w => echo(w + ' kota') )

p3.then( w => { console.log(w) } )


// error handling 

function echo(msg,error){
    return new Promise((resolve, reject )=>{

        setTimeout(()=>{
            error? reject('ups..') : resolve(msg)
        },2000)
    })
}

p = echo('Ala', true )

p2 = p.then( w => w + ' ma ', err => 'Niewiadomo kto ma')

p3 = p2.then( w => echo(w + ' kota'  ) )

p3.then( w => { console.log(w) }, e => console.log(e))